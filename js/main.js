$(function () {
    $('.bucket').hover(function (e) {
        $('.bucket').removeClass('selected');
        $(e.target).addClass('selected');

        var selectedBucket = $(e.target);
        var color = selectedBucket.attr('data-color');

        $('.paintbucket-area-container').css('background-color', color);
        if (color== '#eb690a') {
            $('.btn-cta-primary').css('background-color', '#3e4f57');
        } else {
            $('.btn-cta-primary').css('background-color', '#eb690a');
        }
    });

    // Header, footer, sisu ja hamburger
    $(window).scroll(function() {
        var header = $(".fixed-header");
        var footer = $(".site-inner .fixed-footer");
        var step = $(".step");
        var step2 = $(".step2");
        var step3 = $(".step3");
        var step4 = $(".step4");
        var hamburger = $(".hamburger");

        var scroll = $(window).scrollTop();
    
        if (scroll >= 1800) {
            step4.addClass(".step4 s-enabled");
        } else {
            step4.removeClass(".step4 s-enabled");
        }

        if (scroll >= 1500) {
            step3.addClass(".step3 s-enabled");
        } else {
            step3.removeClass(".step3 s-enabled");
        }

        if (scroll >= 1200) {
            step2.addClass(".step2 s-enabled");
        } else {
            step2.removeClass(".step2 s-enabled");
        }

        if (scroll >= 1000) {
            hamburger.addClass("active-fixed-header");
            header.removeClass("fixed-header").addClass("fixed-header active");
            footer.removeClass(".site-inner .fixed-to-window").addClass(".site-inner .fixed-to-window active");
        } else {
            hamburger.removeClass("active-fixed-header");
            header.removeClass("fixed-header active").addClass('fixed-header');
            footer.removeClass(".site-inner .fixed-to-window active").addClass('.site-inner .fixed-to-window');
        }

        if (scroll >= 800) {
            step.addClass(".step s-enabled");
        } else {
            step.removeClass(".step s-enabled");
        }
    });

    $('.hamburger').click(function() {
       $('body').toggleClass('full-body')
       $('.hamburger').toggleClass('active')
       $('.mobile-main-nav').toggleClass('active')
       $('.mobile-main-nav').toggleClass('animate')
       $('li.show1').toggleClass('mobile')
    });
});
